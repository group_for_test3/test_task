from django.db import models
from django.contrib.auth.models import User


class Task(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    task_name = models.CharField(max_length=40)
    task_description = models.TextField()
    complete_date = models.DateTimeField(null=True)
